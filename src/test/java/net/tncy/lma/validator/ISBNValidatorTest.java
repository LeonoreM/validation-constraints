package net.tncy.lma.validator;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ISBNValidatorTest {

    private static ISBNValidator validator;

    @BeforeClass
    public static void setUpValidator() {
        validator = new ISBNValidator();
    }

    @Test
    public void testValidISBN10() {
        validator.initialize(ISBNFormat.ISBN_10);
        assertTrue(validator.isValid("2-6985-9632-1",null));
    }

    @Test
    public void testValidISBN13() {
        validator.initialize(ISBNFormat.ISBN_13);
        assertTrue(validator.isValid("136-2-6985-9632-1",null));
    }

    @Test
    public void testInvalidISBN10() {
        validator.initialize(ISBNFormat.ISBN_10);
        assertFalse(validator.isValid("2-685-9632-1",null));
    }

    @Test
    public void testInvalidISBN13() {
        validator.initialize(ISBNFormat.ISBN_13);
        assertFalse(validator.isValid("2-6985-9632-1",null));
    }
}
