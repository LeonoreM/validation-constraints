package net.tncy.lma.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.*;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    private ISBNFormat format;

    @Override
    public void initialize(ISBN constraintAnnotation) {
        // initialisation du validateur (traiter les éventuels attributs de l'annotation)
    }

    public void initialize(ISBNFormat format) {
        this.format = format;
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        // Algorithme de validation du numéro ISBN
        if (bookNumber == null)
            return true;    // bonne pratique : considérer la valeur null comme valide (pour ne pas interférer avec
                            // l'annotation @NotNull)

        Pattern pattern;
        Matcher matcher;

        switch(this.format) {
            case ISBN_10:
                pattern = Pattern.compile("^[0-9]-[0-9]{4}-[0-9]{4}-[0-9]$");
                matcher = pattern.matcher(bookNumber);
                if (matcher.matches()) {
                    return true;
                } else {
                    return false;
                }
            case ISBN_13:
                pattern = Pattern.compile("^[0-9]{3}-[0-9]-[0-9]{4}-[0-9]{4}-[0-9]$");
                matcher = pattern.matcher(bookNumber);
                if (matcher.matches()) {
                    return true;
                } else {
                    return false;
                }
        }

        return false;
    }
}
