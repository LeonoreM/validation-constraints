package net.tncy.lma.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ISBNValidator.class)
@Documented
public @interface ISBN {

    String message() default "{net.tncy.validator.constraints.books.ISBN}"; // message d'erreur en cas d'échec de la validation

    Class<?>[] groups() default {}; // définir les groupes de validation auxquels la contrainte appartient,
                                    // quand on veut contextualiser

    Class<? extends Payload>[] payload() default {};    // fournir des données complémentaires généralement utilisées
                                                        // lors de l'exploitation des violations de contraintes

    ISBNFormat format() default ISBNFormat.ISBN_13;

}