package net.tncy.lma.validator;

public enum ISBNFormat {

    ISBN_13, ISBN_10
}
